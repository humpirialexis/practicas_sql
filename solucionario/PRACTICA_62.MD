Ejercicios propuestos
Un club de barrio tiene en su sistema 4 tablas:


"socios": en la cual almacena documento, n�mero, nombre y domicilio de cada socio;


"deportes": que guarda un c�digo, nombre del deporte, d�a de la semana que se dicta y documento del profesor instructor;


"profesores": donde se guarda el documento, nombre y domicilio de los profesores e


"inscriptos": que almacena el n�mero de socio, el c�digo de deporte y si la matricula est� paga o no.



1--Elimine las tablas:


drop table inscriptos;
drop table socios;
drop table deportes;
drop table profesores;



2--Considere que:


un socio puede inscribirse en varios deportes, pero no dos veces en el mismo.
un socio tiene un documento �nico y un n�mero de socio �nico.
un deporte debe tener asignado un profesor que exista en "profesores" o "null" si a�n no tiene un instructor definido.
el campo "dia" de "deportes" puede ser: lunes, martes, miercoles, jueves, viernes o sabado.
el campo "dia" de "deportes" por defecto debe almacenar 'sabado'.
un profesor puede ser instructor de varios deportes o puede no dictar ning�n deporte.
un profesor no puede estar repetido en "profesores".
un inscripto debe ser socio, un socio puede no estar inscripto en ning�n deporte.
una inscripci�n debe tener un valor en socio existente en "socios" y un deporte que exista en "deportes".
el campo "matricula" de "inscriptos" debe aceptar solamente los caracteres 's' o'n'.
si se elimina un profesor de "profesores", el "documentoprofesor" coincidente en "deportes" debe quedar seteado a null.
no se puede eliminar un deporte de "deportes" si existen inscriptos para tal deporte en "inscriptos".
si se elimina un socio de "socios", el registro con "numerosocio" coincidente en "inscriptos" debe eliminarse.


3--Cree las tablas con las restricciones necesarias:


create table profesores(
    documento char(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    constraint PK_profesores_documento
    primary key (documento)
);

create table deportes(
    codigo number(2),
    nombre varchar2(20) not null,
    dia varchar2(9) default 'sabado',
    documentoprofesor char(8),
    constraint CK_deportes_dia_lista
    check (dia in ('lunes','martes','miercoles','jueves','viernes','sabado')),
    constraint PK_deportes_codigo
    primary key (codigo),
    constraint FK_deportes_profesor
    foreign key (documentoprofesor)
    references profesores(documento)
    on delete set null
);

create table socios(
    numero number(4),
    documento char(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    constraint PK_socios_numero
    primary key (numero),
    constraint UQ_socios_documento
    unique (documento)
);

create table inscriptos(
    numerosocio number(4),
    codigodeporte number(2),
    matricula char(1),
    constraint PK_inscriptos_numerodeporte
    primary key (numerosocio,codigodeporte),
    constraint FK_inscriptos_deporte
    foreign key (codigodeporte)
    references deportes(codigo),
    constraint FK_inscriptos_socios
    foreign key (numerosocio)
    references socios(numero)
    on delete cascade,
    constraint CK_matricula_valores
    check (matricula in ('s','n'))
);



4--Ingrese registros en "profesores":


insert into profesores values('21111111','Andres Acosta','Avellaneda 111');
insert into profesores values('22222222','Betina Bustos','Bulnes 222');
insert into profesores values('23333333','Carlos Caseros','Colon 333');



5---Ingrese registros en "deportes". Ingrese el mismo d�a para distintos deportes, un deporte sin d�a confirmado, un deporte sin profesor definido:


insert into deportes values(1,'basquet','lunes',null);
insert into deportes values(2,'futbol','lunes','23333333');
insert into deportes values(3,'natacion',null,'22222222');
insert into deportes values(4,'padle',default,'23333333');
insert into deportes values(5,'tenis','jueves',null);



6--Ingrese registros en "socios":


insert into socios values(100,'30111111','Martina Moreno','America 111');
insert into socios values(200,'30222222','Natalia Norte','Bolivia 222');
insert into socios values(300,'30333333','Oscar Oviedo','Caseros 333');
insert into socios values(400,'30444444','Pedro Perez','Dinamarca 444');



7--Ingrese registros en "inscriptos". Inscriba a un socio en distintos deportes, inscriba varios socios en el mismo deporte.


insert into inscriptos values(100,3,'s');
insert into inscriptos values(100,5,'s');
insert into inscriptos values(200,1,'s');
insert into inscriptos values(400,1,'n');
insert into inscriptos values(400,4,'s');




8--Realice un "join" (del tipo que sea necesario) para mostrar todos los datos del socio junto con el nombre de los deportes en los cuales est� inscripto, el d�a que tiene que asistir y el nombre del profesor que lo instruir� (5 registros)
SELECT s.*, d.nombre AS deporte, d.dia, p.nombre AS profesor
FROM socios s
LEFT JOIN inscriptos i ON s.numero = i.numerosocio
LEFT JOIN deportes d ON i.codigodeporte = d.codigo
LEFT JOIN profesores p ON d.documentoprofesor = p.documento
LIMIT 5;


9--Realice la misma consulta anterior pero incluya los socios que no est�n inscriptos en ning�n deporte (6 registros)
SELECT s.*, d.nombre AS deporte, d.dia, p.nombre AS profesor
FROM socios s
LEFT JOIN inscriptos i ON s.numero = i.numerosocio
LEFT JOIN deportes d ON i.codigodeporte = d.codigo
LEFT JOIN profesores p ON d.documentoprofesor = p.documento;


10--Muestre todos los datos de los profesores, incluido el deporte que dicta y el d�a, incluyendo los profesores que no tienen asignado ning�n deporte, ordenados por documento (4 registros)

SELECT p.*, d.nombre AS deporte, d.dia
FROM profesores p
LEFT JOIN deportes d ON p.documento = d.documentoprofesor
ORDER BY p.documento;

11--Muestre todos los deportes y la cantidad de inscriptos en cada uno de ellos, incluyendo aquellos deportes para los cuales no hay inscriptos, ordenados por nombre de deporte (5 registros)

SELECT d.*, COUNT(i.numerosocio) AS cantidad_inscriptos
FROM deportes d
LEFT JOIN inscriptos i ON d.codigo = i.codigodeporte
GROUP BY d.codigo, d.nombre, d.dia
ORDER BY d.nombre;

12--Muestre las restricciones de "socios"

SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'SOCIOS';

13--Muestre las restricciones de "deportes"

SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'DEPORTES';

14--- Obtenga informaci�n sobre la restricci�n "foreign key" de "deportes"
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'DEPORTES' AND constraint_type = 'R';


15--Muestre las restricciones de "profesores"

SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'PROFESORES';

16---Muestre las restricciones de "inscriptos"

SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'INSCRIPTOS';

17--Consulte "user_cons_columns" y analice la informaci�n retornada sobre las restricciones de "inscriptos"

SELECT constraint_name, column_name
FROM user_cons_columns
WHERE table_name = 'INSCRIPTOS';

18--Elimine un profesor al cual haga referencia alg�n registro de "deportes"

DELETE FROM profesores
WHERE documento = '21111111';

19--Vea qu� sucedi� con los registros de "deportes" cuyo "documentoprofesor" exist�a en "profesores"
---Fue seteado a null porque la restricci�n "foreign key" sobre "documentoprofesor" de "deportes" fue definida "on delete set null".

SELECT * FROM deportes;

20--Elimine un socio que est� inscripto

DELETE FROM socios WHERE numero = 12345;

21--Vea qu� sucedi� con los registros de "inscriptos" cuyo "numerosocio" exist�a en "socios"
--Fue eliminado porque la restricci�n "foreign key" sobre "numerosocio" de "inscriptos" fue definida "on delete cascade".

SELECT * FROM inscriptos;

22--Intente eliminar un deporte para el cual haya inscriptos
--Mensaje de error porque la restricci�n "foreign key sobre "codigodeporte" de "inscriptos" fue establecida "no action".
DELETE FROM deportes WHERE codigo = 100;


23--Intente eliminar la tabla "socios"
--No puede eliminarse, mensaje de error, una "foreign key" sobre "inscriptos" hace referencia a esta tabla.
DROP TABLE socios;


24--Elimine la tabla "inscriptos"

DROP TABLE inscriptos;

25--Elimine la tabla "socios"

DROP TABLE socios;

26--Intente eliminar la tabla "profesores"
--No puede eliminarse, mensaje de error, una "foreign key" sobre "deportes" hace referencia a esta tabla.
DROP TABLE profesores;


27--Elimine la tabla "deportes"

DROP TABLE deportes;

28--Elimine la tabla "profesores"
DROP TABLE profesores;

