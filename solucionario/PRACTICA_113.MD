Ejercicios propuestos

Un comercio almacena los datos de los art�culos que tiene para la venta en una tabla denominada "articulos". En otra tabla denominada "pedidos" almacena el c�digo de cada art�culo y la cantidad que necesita solicitar a los mayoristas. En una tabla llamada "controlPrecios" almacena la fecha, el c�digo del art�culo y ambos precios (antiguo y nuevo).

01 Elimine las tablas:

drop table articulos;
drop table pedidos;
drop table controlPrecios;




02 Cree las tablas con las siguientes estructuras:

create table articulos(
    codigo number(4),
    descripcion varchar2(40),
    precio number (6,2),
    stock number(4)
);

create table pedidos(
    codigo number(4),
    cantidad number(4)
);

create table controlPrecios(
    fecha date,
    codigo number(4),
    anterior number(6,2),
    nuevo number(6,2)
);




03 Ingrese algunos registros en "articulos":

insert into articulos values(100,'cuaderno rayado 24h',4.5,100);
insert into articulos values(102,'cuaderno liso 12h',3.5,150);
insert into articulos values(104,'lapices color x6',8.4,60);
insert into articulos values(160,'regla 20cm.',6.5,40);
insert into articulos values(173,'compas xxx',14,35);
insert into articulos values(234,'goma lapiz',0.95,200);




04 Ingrese en "pedidos" todos los c�digos de "articulos", con "cantidad" cer

insert into pedidos
select codigo, 0 from articulos;



05 Active el paquete "dbms_output":

set serveroutput on;
execute dbms_output.enable(20000);




06 Cada vez que se disminuye el stock de un art�culo de la tabla "articulos", se debe incrementar la misma cantidad de ese art�culo en "pedidos" y cuando se incrementa en "articulos", se debe disminuir la misma cantidad en "pedidos". Si se ingresa un nuevo art�culo en "articulos", debe agregarse un registro en "pedidos" con "cantidad" cero. Si se elimina un registro en "articulos", debe eliminarse tal art�culo de "pedidos". Cree un trigger para los tres eventos (inserci�n, borrado y actualizaci�n), a nivel de fila, sobre "articulos", para los campos "stock" y "precio", que realice las tareas descriptas anteriormente, si el campo modificado es "stock". Si el campo modificado es "precio", almacene en la tabla "controlPrecios", la fecha, el c�digo del art�culo, el precio anterior y el nuevo.

El trigger muestra el mensaje "Trigger activado" cada vez que se dispara; en cada "if" muestra un segundo mensaje que indica cu�l condici�n se ha cumplido.

create or replace trigger tr_stock_precio
before update or insert or delete on articulos
for each row
begin
  if updating and :new.stock < :old.stock then
    update pedidos
    set cantidad = cantidad + (:old.stock - :new.stock)
    where codigo = :old.codigo;
    dbms_output.put_line('Trigger activado: Disminuci�n de stock');
  elsif updating and :new.stock > :old.stock then
    update pedidos
    set cantidad = cantidad - (:new.stock - :old.stock)
    where codigo = :new.codigo;
    dbms_output.put_line('Trigger activado: Incremento de stock');
  elsif inserting then
    insert into pedidos values (:new.codigo, 0);
    dbms_output.put_line('Trigger activado: Inserci�n de art�culo');
  elsif deleting then
    delete from pedidos where codigo = :old.codigo;
    dbms_output.put_line('Trigger activado: Borrado de art�culo');
  end if;
  
  if updating and :new.precio != :old.precio then
    insert into controlPrecios values (sysdate, :new.codigo, :old.precio, :new.precio);
    dbms_output.put_line('Trigger activado: Actualizaci�n de precio');
  end if;
end;
/




07 Disminuya el stock del art�culo "100" a 30 Un mensaje muestra que el trigger se ha disparado actualizando el "stock".

update articulos
set stock = 30
where codigo = 100;



08 Verifique que el trigger se dispar� consultando la tabla "pedidos" (debe aparecer "70" en "cantidad" en el registro correspondiente al art�culo "100")

select * from pedidos;



09 Ingrese un nuevo art�culo en "articulos" Un mensaje muestra que el trigger se ha disparado por una inserci�n.

insert into articulos values (280, 'nuevo articulo', 10.5, 50);



10 Verifique que se ha agregado un registro en "pedidos" con c�digo "280" y cantidad igual a 0

select * from pedidos where codigo = 280;



11 Elimine un art�culo de "articulos" Un mensaje muestra que el trigger se ha disparado por un borrado.

delete from articulos where codigo = 160;



12 Verifique que se ha borrado el registro correspondiente al art�culo con c�digo "234" en "pedidos"

select * from pedidos where codigo = 160;




13 Modifique el precio de un art�culo Un mensaje muestra que el trigger se ha disparado por una actualizaci�n de precio.

update articulos
set precio = 5.75
where codigo = 104;



14 Verifique que se ha agregado un registro en "controlPrecios"

select * from controlPrecios;



15 Modifique la descripci�n de un art�culo El trigger no se ha disparado, no aparece mensaje.

update articulos
set descripcion = 'lapices de colores'
where codigo = 104;



16 Modifique el precio, stock y descripcion de un art�culo Un mensaje muestra que el trigger se ha disparado por una actualizaci�n de stock y otra de precio. La actualizaci�n de "descripcion" no dispar� el trigger.

update articulos
set stock = 20,
    precio = 9.99,
    descripcion = 'nueva descripcion'
where codigo = 234;



17 Verifique que se ha agregado un registro en "controlPrecios" y se ha modificado el campo "cantidad" con el valor "5"

select * from controlPrec



18 Modifique el stock de varios art�culos en una sola sentencia Cuatro mensajes muestran que el trigger se ha disparado 4 veces, por actualizaciones de stock.

update articulos
set stock = stock - 10
where codigo in (100, 102, 104, 173);



19 Verifique que se han modificado 4 registros en "pedidos"

select * from pedidos;




20 Modifique el precio de varios art�culos en una sola sentencia Cuatro mensajes muestran que el trigger se ha disparado 4 veces, por actualizaciones de precio.

update articulos
set precio = precio * 1.1
where codigo in (100, 102, 104, 173);



21 Verifique que se han agregado 4 nuevos registros en "controlPrecios"

select * from controlPrecios;



22 Elimine varios art�culos en una sola sentencia Cuatro mensajes muestran que el trigger se ha disparado 4 veces, por borrado de registros.

delete from articulos where codigo in (100, 102, 104, 173);



23 Verifique que se han eliminado 4 registros en "pedidos"

select * from pedidos;