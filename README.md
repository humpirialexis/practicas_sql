[1. Crear tablas (create table - describe - all_tables - drop table)](solucionario/PRACTICA_01.MD)

[2. Ingresar registros (insert into- select)](solucionario/PRACTICA_2.MD)

[3. Tipos de datos](solucionario/PRACTICA_03.MD)

[4. Recuperar algunos campos (select)](solucionario/PRACTICA_04.MD)

[5. Recuperar algunos registros (where)](solucionario/PRACTICA_05.MD)

[6. Operadores relacionales](solucionario/PRACTICA_06.MD)

[7. Borrar registros (delete)](solucionario/PRACTICA_07.MD)

[8. Actualizar registros (update)](solucionario/PRACTICA_08.MD)

[9. Comentarios](solucionario/PRACTICA_09.MD)

[10. Valores nulos (null)](solucionario/PRACTICA_10.MD)

[11. Operadores relacionales (is null)](solucionario/PRACTICA_11.MD)

[12. Clave primaria (primary key)](solucionario/PRACTICA_12.MD)

[13. Vaciar la tabla (truncate table)](solucionario/PRACTICA_13.MD)

[14. Tipos de datos alfanuméricos](solucionario/PRACTICA_14.MD)

[15. Tipos de datos numéricos](solucionario/PRACTICA_15.MD)

[16. Ingresar algunos campos](solucionario/PRACTICA_16.MD)

[17. Valores por defecto (default)](solucionario/PRACTICA_17.MD)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](solucionario/PRACTICA_18.MD)

[19.  Alias (encabezados de columnas)](solucionario/PRACTICA_19.MD)

[20. Funciones string](solucionario/PRACTICA_20.MD)

[21. Funciones matemáticas](solucionario/PRACTICA_21.MD)

[22. Funciones de fechas y horas](solucionario/PRACTICA_22.MD)

[23. Ordenar registros (order by)](solucionario/PRACTICA_23.MD)

[24. Operadores lógicos (and - or - not)](solucionario/PRACTICA_24.MD)

[25. Otros operadores relacionales (between)](solucionario/PRACTICA_25.MD)

[26. Otros operadores relacionales (in)](solucionario/PRACTICA_26.MD)

[27. Búsqueda de patrones (like - not like)](solucionario/PRACTICA_27.MD)

[28. Contar registros (count)](solucionario/PRACTICA_.28MD)

[29. Funciones de grupo (count - max - min - sum - avg)](solucionario/PRACTICA_29.MD)

[30. Agrupar registros (group by)](solucionario/PRACTICA_30.MD)

[31. Seleccionar grupos (Having)](solucionario/PRACTICA_31.MD)

[32. Registros duplicados (Distinct)](solucionario/PRACTICA_32.MD)

[33. Clave primaria compuesta](solucionario/PRACTICA_33.MD)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](solucionario/PRACTICA_34.MD)

[35. Alterar secuencia (alter sequence)](solucionario/PRACTICA_35.MD)

[36. Integridad de datos](solucionario/PRACTICA_36.MD)

[37. Restricción primary key](solucionario/PRACTICA_37.MD)

[38. Restricción unique](solucionario/PRACTICA_38.MD)

[39. Restriccioncheck](solucionario/PRACTICA_39.MD)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](solucionario/PRACTICA_40.MD)

[41. Restricciones: información (user_constraints - user_cons_columns)](solucionario/PRACTICA_41.MD)

[42. Restricciones: eliminación (alter table - drop constraint)](solucionario/PRACTICA_42.MD)

[43. Indices](solucionario/PRACTICA_43.MD)

[44. Indices](solucionario/PRACTICA_44.MD)

[45. Indices (Crear . Información)](solucionario/PRACTICA_45.MD)

[46. Indices (eliminar)](solucionario/PRACTICA_46.MD)

[47. Varias tablas (join)](solucionario/PRACTICA_47.MD)

[48. Combinación interna (join)](solucionario/PRACTICA_48.MD)

[49. Combinación externa izquierda (left join)](solucionario/PRACTICA_49.MD)

[50. Combinación externa derecha (right join)](solucionario/PRACTICA_50.MD)

[51. Combinación externa completa (full join)](solucionario/PRACTICA_51.MD)

[52. Combinaciones cruzadas (cross)](solucionario/PRACTICA_52.MD)

[53. Autocombinación](solucionario/PRACTICA_53.MD)

[54. Combinaciones y funciones de agrupamiento](solucionario/PRACTICA_54.MD)

[55. Combinar más de 2 tablas](solucionario/PRACTICA_55.MD)

[56. Otros tipos de combinaciones](solucionario/PRACTICA_56.MD)

[57. Clave foránea](solucionario/PRACTICA_57.MD)

[58. Restricciones (foreign key)](solucionario/PRACTICA_58.MD)

[59. Restricciones foreign key en la misma tabla](solucionario/PRACTICA_59.MD)

[60. Restricciones foreign key (eliminación)](solucionario/PRACTICA_60.MD)

[61. Restricciones foreign key deshabilitar y validar](solucionario/PRACTICA_61.MD)

[62. Restricciones foreign key (acciones)](solucionario/PRACTICA_62.MD)

[63. Información de user_constraints](solucionario/PRACTICA_63.MD)

[64. Restricciones al crear la tabla](solucionario/PRACTICA_64.MD)

[65. Unión](solucionario/PRACTICA_65.MD)

[66. Intersección](solucionario/PRACTICA_66.MD)

[67. Minus](solucionario/PRACTICA_67.MD)

[68. Agregar campos (alter table-add)](solucionario/PRACTICA_68.MD)

[69. Modificar campos (alter table - modify)](solucionario/PRACTICA_69.MD)

[70. Eliminar campos (alter table - drop)](solucionario/PRACTICA_70.MD)

[71. Agregar campos y restricciones (alter table)](solucionario/PRACTICA_71.MD)

[72. Subconsultas](solucionario/PRACTICA_72.MD)

[73. Subconsultas como expresion](solucionario/PRACTICA_73.MD)

[74. Subconsultas con in](solucionario/PRACTICA_74.MD)

[75. Subconsultas any- some - all](solucionario/PRACTICA_75.MD)

[76. Subconsultas correlacionadas](solucionario/PRACTICA_76.MD)

[77. Exists y No Exists](solucionario/PRACTICA_77.MD)

[78. Subconsulta simil autocombinacion](solucionario/PRACTICA_78.MD)

[79. Subconsulta conupdate y delete](solucionario/PRACTICA_79.MD)

[80. Subconsulta e insert](solucionario/PRACTICA_80.MD)

[81. Crear tabla a partir de otra (create table-select)](solucionario/PRACTICA_81.MD)

[82. Vistas (create view)](solucionario/PRACTICA_82.MD)

[83. Vistas (información)](solucionario/PRACTICA_83.MD)

[84. Vistas eliminar (drop view)](solucionario/PRACTICA_84.MD)

[85. Vistas (modificar datos a través de ella)](solucionario/PRACTICA_85.MD)

[86. Vistas (with read only)](solucionario/PRACTICA_86.MD)

[87. Vistas modificar (create or replace view)](solucionario/PRACTICA_87.MD)

[88. Vistas (with check option)](solucionario/PRACTICA_88.MD)

[89. Vistas (otras consideraciones: force)](solucionario/PRACTICA_89.MD)

[90. Vistas materializadas (materialized view)](solucionario/PRACTICA_90.MD)

[91. Procedimientos almacenados](solucionario/PRACTICA_91.MD)

[92. Procedimientos Almacenados (crear- ejecutar)](solucionario/PRACTICA_92.MD)

[93. Procedimientos Almacenados (eliminar)](solucionario/PRACTICA_93.MD)

[94. Procedimientos almacenados (parámetros de entrada)](solucionario/PRACTICA_94.MD)

[95. Procedimientos almacenados (variables)](solucionario/PRACTICA_95.MD)

[96. Procedimientos Almacenados (informacion)](solucionario/PRACTICA_96.MD)

[97. Funciones](solucionario/PRACTICA_97.MD)

[98. Control de flujo (if)](solucionario/PRACTICA_98.MD)

[99. Control de flujo (case)](solucionario/PRACTICA_99.MD)

[100. Control de flujo (loop)](solucionario/PRACTICA_100.MD)

[101. Control de flujo (for)](solucionario/PRACTICA_101.MD)

[102. Control de flujo (while loop)](solucionario/PRACTICA_102.MD)

[103. Disparador (trigger)](solucionario/PRACTICA_103.MD)

[104. Disparador (información)](solucionario/PRACTICA_104.MD)

[105. Disparador de inserción a nivel de sentencia](solucionario/PRACTICA_105.MD)

[106. Disparador de insercion a nivel de fila (insert trigger for each row)](solucionario/PRACTICA_106.MD)

[107. Disparador de borrado (nivel de sentencia y de fila)](solucionario/PRACTICA_107.MD)

[108. Disparador de actualizacion a nivel de sentencia (update trigger)](solucionario/PRACTICA_108.MD)

[109. Disparador de actualización a nivel de fila (update trigger)](solucionario/PRACTICA_109.MD)

[110. Disparador de actualización - lista de campos (update trigger)](solucionario/PRACTICA_110.MD)

[111. Disparador de múltiples eventos](solucionario/PRACTICA_111.MD)

[112. Disparador (old y new)](solucionario/PRACTICA_112.MD)

[113. Disparador condiciones (when)](solucionario/PRACTICA_113.MD)

[114. Disparador de actualizacion - campos (updating)](solucionario/PRACTICA_114.MD)

[115. Disparadores (habilitar y deshabilitar)](solucionario/PRACTICA_115.MD)

[116. Disparador (eliminar)](solucionario/PRACTICA_116.MD)

[117. Errores definidos por el usuario en trigger](solucionario/PRACTICA_117.MD)

[118. Seguridad y acceso a Oracle](solucionario/PRACTICA_118.MD)

[119. Usuarios (crear)](solucionario/PRACTICA_119.MD)

[120. Permiso de conexión](solucionario/PRACTICA_120.MD)

[121. Privilegios del sistema (conceder)](solucionario/PRACTICA_121.MD)

[122. Privilegios del sistema (with admin option)](solucionario/PRACTICA_122.MD)